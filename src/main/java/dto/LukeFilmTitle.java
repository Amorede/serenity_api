package dto;

import lombok.Data;

import java.util.List;

@Data
public class LukeFilmTitle {

    final String title;
}
