package dto;

import java.util.ArrayList;
import java.util.List;

public class StarWarsUserCompound {


    public  StarWarsUser user;
    public  List<String> filmTitles;


    public StarWarsUserCompound(StarWarsUser user, List<String> filmTitles) {
        this.user = user;
        this.filmTitles = filmTitles;
    }

}
