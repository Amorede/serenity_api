package core.pages;

import org.openqa.selenium.WebDriver;

public class RozetkaMainPage extends AbstractPage {


    public RozetkaMainPage(final WebDriver driver) {
        super(driver);
    }

    private final String LOCAL_SEARCH_INPUT_FIELD = "//input[@class='rz-header-search-input-text passive']";
    private final String SEARCH_INPUT_FIELD = "//input[@class='search__input ng-untouched ng-pristine ng-valid']";
    private final String LOCAL_SEARCH_BUTTON = "//button[@class='btn-link-i js-rz-search-button']";
    private final String SEARCH_BUTTON = "//button[@class='button button_color_green search__button']";


    public void setTextToSearchField(final String text) {
        if (findBy(SEARCH_INPUT_FIELD).isPresent()) {
            findBy(SEARCH_INPUT_FIELD).then().type(text);
        } else {
            findBy(LOCAL_SEARCH_INPUT_FIELD).type(text);
        }
    }

    public void clickOnSearchButton() {
        if (findBy(SEARCH_BUTTON).isPresent()) {
            findBy(SEARCH_BUTTON).then().click();
        } else {
            findBy(LOCAL_SEARCH_BUTTON).then().click();
        }
    }

}
