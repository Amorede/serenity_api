package core.pages.p;

import lombok.Data;

@Data
public class Meta{
	private int duration;
	private int total;
	private int of;
	private int start;
	private int end;
	private Object maxScore;

	@Override
 	public String toString(){
		return 
			"Meta{" + 
			"duration = '" + duration + '\'' + 
			",total = '" + total + '\'' + 
			",of = '" + of + '\'' + 
			",start = '" + start + '\'' + 
			",end = '" + end + '\'' + 
			",maxScore = '" + maxScore + '\'' + 
			"}";
		}
}
