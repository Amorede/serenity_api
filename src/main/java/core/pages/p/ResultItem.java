package core.pages.p;

import lombok.Data;

import java.util.List;

@Data
public class ResultItem{
	private String sourceId;
	private String thumbnail;
	private String byLine;
	private String lastModifiedDate;
	private String section;
	private String language;
	private String source;
	private String type;
	private String body;
	private String url;
	private List<TagsItem> tags;
	private String path;
	private String mappedSection;
	private String firstPublishDate;
	private Object location;
	private String id;
	private List<String> contributors;
	private String headline;
	private double trt;

	@Override
 	public String toString(){
		return 
			"ResultItem{" + 
			"sourceId = '" + sourceId + '\'' + 
			",thumbnail = '" + thumbnail + '\'' + 
			",byLine = '" + byLine + '\'' + 
			",lastModifiedDate = '" + lastModifiedDate + '\'' + 
			",section = '" + section + '\'' + 
			",language = '" + language + '\'' + 
			",source = '" + source + '\'' + 
			",type = '" + type + '\'' + 
			",body = '" + body + '\'' + 
			",url = '" + url + '\'' + 
			",tags = '" + tags + '\'' + 
			",path = '" + path + '\'' + 
			",mappedSection = '" + mappedSection + '\'' + 
			",firstPublishDate = '" + firstPublishDate + '\'' + 
			",location = '" + location + '\'' + 
			",_id = '" + id + '\'' + 
			",contributors = '" + contributors + '\'' + 
			",headline = '" + headline + '\'' + 
			",trt = '" + trt + '\'' + 
			"}";
		}
}