package core.pages.p;

import core.pages.AbstractPage;
import org.openqa.selenium.WebDriver;

public class RozetkaWishlist extends AbstractPage {

    public RozetkaWishlist(final WebDriver driver) {
        super(driver);
    }

    private final String TITLE_FROM_WISHLIST = "//a[@class='novisited g-title-link wishlist-g-i-title-link']";


    public String getTextFromTitleFromTheWishlist(){
        return findBy(TITLE_FROM_WISHLIST).getText();
    }

}
