package core.pages.p;

import lombok.Data;

import java.util.List;
@Data
public class CNNPage{
	private List<ResultItem> result;
	private Meta meta;

	@Override
 	public String toString(){
		return 
			"CNNPage{" + 
			"result = '" + result + '\'' + 
			",meta = '" + meta + '\'' + 
			"}";
		}
}