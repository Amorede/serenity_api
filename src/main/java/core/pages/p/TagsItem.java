package core.pages.p;

import lombok.Data;

@Data
public class TagsItem{
	private String id;

	@Override
 	public String toString(){
		return 
			"TagsItem{" + 
			"id = '" + id + '\'' + 
			"}";
		}
}
