package core.pages;

import net.bytebuddy.asm.Advice;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;

public class GoogleMainPage extends PageObject {

    private static final String SEARCH_INPUT = "//input[@name='q']";

    public GoogleMainPage(final WebDriver driver) {
        super(driver);
    }

    public String getCurrentTitle() {
        return getTitle();
    }

    public void setTextToSearchField(final String text) {
        findBy(SEARCH_INPUT).then().type(text);
    }

    public void onSearchButtonClicked() {
        find().then().click();
    }

    public String getTextFromSearchField() {
        return findBy(SEARCH_INPUT).then().getTextValue();
    }

    public void clickEnterButton(){
        findBy(SEARCH_INPUT).then().submit();
    }


}
