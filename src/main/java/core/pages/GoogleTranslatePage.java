package core.pages;

import core.panels.LanguagePanel;
import core.panels.TranslatePanel;
import org.openqa.selenium.WebDriver;

public class GoogleTranslatePage extends AbstractPage {

    private static final String TRANSLATE_PANEL = "//div[@class='homepage-content-wrap']";
    private static final String CHOOSE_LANGUAGE_PANEL = "//div[@class='page tlid-language-picker-page language-picker-page']";



    public GoogleTranslatePage(final WebDriver driver) {
        super(driver);
    }


    public TranslatePanel getTranslatePanel(){
        return new TranslatePanel(findBy(TRANSLATE_PANEL), this);
    }

    public LanguagePanel getLanguagePanel(){
        return new LanguagePanel(findBy(CHOOSE_LANGUAGE_PANEL), this);
    }



}
