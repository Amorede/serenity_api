package core.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class GoogleResultPage extends PageObject {

    @FindBy(xpath = "//div[@class='srg']//h3[@class='LC20lb']")
    private static List<WebElementFacade> resultList;

    public static List<WebElementFacade> getResultList() {
        return resultList;
    }


    public GoogleResultPage(final WebDriver driver) {
        super(driver);
    }


    public List<String> getSearchResultText(){
        final List<String> listOfResult = new ArrayList<>();
        for (WebElementFacade result : resultList){
            listOfResult.add(result.getText().toLowerCase());
        }
        return listOfResult;
    }


}
