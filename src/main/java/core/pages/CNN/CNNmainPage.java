package core.pages.CNN;

import core.pages.AbstractPage;
import org.openqa.selenium.WebDriver;

public class CNNmainPage extends AbstractPage {

    public CNNmainPage(final WebDriver driver) {
        super(driver);
    }

    private final String SEARCH_ICON = "//div[@id = 'search-button']";
    private final String SEARCH_INPUT_FIELD = "//input[@class = 'search__input-field']";


    public void setSearchTextAndReturnResultPage(final String text) {
        findBy(SEARCH_ICON).click();
        findBy(SEARCH_INPUT_FIELD).type(text).then().submit();
    }
}
