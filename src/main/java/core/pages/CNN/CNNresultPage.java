package core.pages.CNN;

import core.pages.AbstractPage;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class CNNresultPage extends AbstractPage {

    public CNNresultPage(final WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//h3[@class='cnn-search__result-headline']")
    private List<WebElement> titles;

    @FindBy(xpath = "//div[@class='cnn-search__result-body']")
    private List<WebElement> bodies;
    
    public List<String> getTitles(){
        List<String> titlesNaming = new ArrayList<>();
        for (WebElement cell : titles) {
            titlesNaming.add(cell.getText());
        }
        return titlesNaming;
    }

    public List<String> getBodies(){
        List<String> bodiesNaming = new ArrayList<>();
        for (WebElement cell : bodies) {
            bodiesNaming.add(cell.getText());
        }
        return bodiesNaming;
    }

}
