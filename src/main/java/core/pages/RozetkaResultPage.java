package core.pages;

import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class RozetkaResultPage extends AbstractPage {

    public RozetkaResultPage(final WebDriver driver) {
        super(driver);
    }

    private final String LOCAL_ADD_TO_WISHLIST_BUTTON = "//div[@class='g-i-tile g-i-tile-catalog'][1]//li[@name='wishlists_catalog_new_tile']";
    private final String ADD_TO_WISHLIST_BUTTON = "//div[@class='g-i-tile g-i-tile-catalog'][1]//li[@name='wishlists_catalog_new_tile']";
    private final String INPUT_WISHLIST_NAME = "//input[@name='wishlist_title']";
    private final String INPUT_WISHLIST_EMAIL = "//input[@name='login']";
    private final String BUTTON_FIRST_POP_UP_CONFIRM = "//span[@class='btn-link btn-link-green wishlists-submit-btn']";
    private final String INPUT_NAME_OF_CUSTOMER = "//input[@name='fio']";
    private final String BUTTON_SECOND_POP_UP = "//span[@class='btn-link btn-link-blue double-auth-submit']";
    private final String BUTTON_WISHLIST = "//div[@id='wishlist']";
    private final String FIRST_PRODUCT_CONTAINER = "//div[@class='g-i-tile-l clearfix']//div[@class='g-i-tile g-i-tile-catalog'][1]";
    private final String CONFIRM_EMAIL_POPUP = "//a[@class='confirm-email-popup-i-close novisited']";


    public void hoverElement(WebElementFacade element) {
        Actions builder = new Actions(getDriver());
        Actions hoverOverLocationSelector = builder.moveToElement(element);
        hoverOverLocationSelector.perform();
    }

    public void clickOnAddToWishList(){
        Actions builder = new Actions(getDriver());
        Actions hoverOverLocationSelector = builder.moveToElement(findBy(FIRST_PRODUCT_CONTAINER));
        hoverOverLocationSelector.perform();

        if(findBy(ADD_TO_WISHLIST_BUTTON).isPresent()) {
            findBy(ADD_TO_WISHLIST_BUTTON).click();
        }else {
            findBy(LOCAL_ADD_TO_WISHLIST_BUTTON).click();
        }
    }

    public void setFirstPopUp (final String groupName, final String email) {

        findBy(INPUT_WISHLIST_NAME).then().type(groupName);
        findBy(INPUT_WISHLIST_EMAIL).then().type(email);
        findBy(BUTTON_FIRST_POP_UP_CONFIRM).then().click();
    }

    public void setSecondPopUp (final String name) {
        findBy(INPUT_NAME_OF_CUSTOMER).then().type(name);
        findBy(BUTTON_SECOND_POP_UP).then().click();
    }

    public void clickOnWishListButton(){
        if (findBy(CONFIRM_EMAIL_POPUP).isCurrentlyVisible()){
            findBy(CONFIRM_EMAIL_POPUP).click();
            findBy(BUTTON_WISHLIST).then().click();
        } else {
        findBy(BUTTON_WISHLIST).then().click();
    }

    }

}
