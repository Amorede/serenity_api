package core.ws;

import core.pages.AbstractPage;
import org.openqa.selenium.WebDriver;

public class SWAPIPage  extends AbstractPage {


    private final String RESULT_FIELD = "//pre[@id='interactive_output']";


    public SWAPIPage(final WebDriver driver) {
        super(driver);
    }


    public String getResultText(){
        String result;
        return result = findBy(RESULT_FIELD).getText();
    }

}
