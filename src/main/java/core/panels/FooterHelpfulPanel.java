package core.panels;

import core.pages.AbstractPage;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;
import java.util.stream.Collectors;

public class FooterHelpfulPanel extends AbstractPanel {

    private static final String HELPFUL_LINKS = ".//div[@class='footer-section__col']//div[@class='helpfulLinks glb-helpful-links']//div[@data-module-type='HelpfulLinks']//a[@class='footer-section__menu-link needsclick ']";

    public FooterHelpfulPanel(final WebElementFacade panelLocator, final AbstractPage rootPage) {
        super(panelLocator, rootPage);
    }

    public List<String> getHelpfulLinksText(){
        final List<WebElementFacade> helpfulLinksItems = getRootPage().findAll(HELPFUL_LINKS);
        return helpfulLinksItems.stream().map(item -> item.getText()).collect(Collectors.toList());
    }
}


//    @FindBy(xpath = "//div[@class='footer-section__col']//div[@class='helpfulLinks glb-helpful-links']//div[@data-module-type='HelpfulLinks']//a[@class='footer-section__menu-link needsclick ']")
//    List<WebElementFacade> helpfulLinksElements;

//        List<String> helpful = new ArrayList<>();
//        for (WebElementFacade cell: helpfulLinksElements) {
//            helpful.add(cell.getText());
//        }

//        return helpful;
