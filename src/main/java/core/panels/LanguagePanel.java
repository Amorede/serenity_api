package core.panels;

import core.pages.AbstractPage;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;

public class LanguagePanel extends AbstractPanel {

    private static final String SOURCE_LANGUAGE = ".//div[@class='language-list-unfiltered-langs-sl_list']//div[contains(text(),'%s')]";
    private static final String TARGET_LANGUAGE = ".//div[@class='language-list-unfiltered-langs-tl_list']//div[contains(text(),'%s')]";
    private static final String OPEN_SOURCE_LANGUAGE_LIST = ".//div[@class='sl-more tlid-open-source-language-list']";
    private static final String OPEN_TARGET_LANGUAGE_LIST = ".//div[@class='tl-more tlid-open-target-language-list']";


    public LanguagePanel(final WebElementFacade panelLocator, final AbstractPage rootPage) {
        super(panelLocator, rootPage);
    }


    public void selectSourceLanguage(final String language){
        getRootPage().findBy(OPEN_SOURCE_LANGUAGE_LIST).click();
        selectRequiredParameter(SOURCE_LANGUAGE, language);
    }

    public void selectTargetLanguage(final String language) {
            getRootPage().findBy(OPEN_TARGET_LANGUAGE_LIST).click();
            selectRequiredParameter(TARGET_LANGUAGE, language);

    }

    private void selectRequiredParameter(final String shortPath, final String language){
        final String languageAlreadySelectedCondition = "jfk-button-checked";
        final String fullPath = String.format(shortPath, language);
        final WebElementFacade languageToBeClickable = getRootPage().findBy(fullPath);
        if (!languageToBeClickable.getAttribute("class").contains(languageAlreadySelectedCondition)){
            languageToBeClickable.then().click();
        }
    }
}
