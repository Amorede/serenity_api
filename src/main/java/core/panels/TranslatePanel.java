package core.panels;

import core.pages.AbstractPage;
import net.serenitybdd.core.pages.WebElementFacade;

public class TranslatePanel extends AbstractPanel {

    private static final String INPUT_TEXT_AREA = ".//textarea[@id='source']";
    private static final String TARGET_TEXT_AREA = ".//span[contains(@class, 'tlid-translation translation')]//span";


    public TranslatePanel( final WebElementFacade panelLocator, final AbstractPage rootPage) {
        super(panelLocator, rootPage);
    }

    public void typeTextToTranslate (final String text){
        getRootPage().findBy(INPUT_TEXT_AREA).then().type(text);
    }

    public String getTranslatedText(){
        return getRootPage().findBy(TARGET_TEXT_AREA).then().getText();
    }



}
