Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: ability to delete

Given delete post with id '2'
Then check if post deleted


Scenario: post new resource

Given create new post with following details:
|title        |author |
|Create  post |Amorede|
Then created post with id '2' must be avaliable with next details:
|title        |author |
|Create  post |Amorede|

Scenario: change created resource

Given modified created rosource with id '2':
|title          |author    |
|modified  post |by Amorede|
Then modified post with id '2' must be avaliable with next details:
|title          |author    |
|modified  post |by Amorede|
