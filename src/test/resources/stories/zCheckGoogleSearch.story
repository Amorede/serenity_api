Meta:
@GoogleCheck

Narrative:
In order to check Google Search functionality
As a user
I want to be able to see the found results

Scenario: Verify Google Search

Given user opened Google page, using following link: 'https://www.google.com/'
When set text 'anacondaz' to search field
Then check text 'anacondaz' is set to search field
Then click enter button
Then check 'anacondaz' search result