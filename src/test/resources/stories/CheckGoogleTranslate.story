Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: Verify that user is able to translate a custom word, using a different languages
Given user is opened a 'https://translate.google.com/'
When user choose '<sourceLanguage>' as a source


And user choose '<targetLanguage>' as a target
And user type '<inputWord>' into the source input
Then user should see next translated words '<translatedResult>'

Examples:
| sourceLanguage | targetLanguage | inputWord | translatedResult |
| Russian        | Spanish        | тесты     | tests            |
| English        | Russian        | tests     | тесты            |
