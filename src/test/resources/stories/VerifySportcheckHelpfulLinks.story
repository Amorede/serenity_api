Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: scenario description


Given opened a Sportcheck page, by URL: 'https://www.sportchek.ca/'
Then helpful links should be displayed: 'Store Locator, Store Locations, Careers, Shop Sportchek.ca, Shop Clearance Deals, Boxing Day Sale, Chek Advice, Gift Cards, Find What Moves You, About Us, Jumpstart, Join Our Affiliate Program, Retail Store Services, Shop by Appointment'