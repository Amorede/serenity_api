package serenity.steps.CNN;

import core.pages.CNN.CNNmainPage;
import core.pages.CNN.CNNresultPage;
import core.pages.p.CNNPage;
import io.restassured.http.ContentType;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.List;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;

public class CNNController extends ScenarioSteps {

    private CNNmainPage cnNmainPage;
    private CNNresultPage cnNresultPage;
    private CNNPage cnnPage;


    private static final String CNN_SERVER_URL = "https://search.api.cnn.io";
    private static final String SEARCH_REQUEST = "/content?q=%s&size=10";


    public void openCNNmainPage(final String url){
        cnNmainPage.openAt(url);
    }


    public void setSearchtextAndOpenResultPage(final String text) {
        cnNmainPage.setSearchTextAndReturnResultPage(text);
    }


    public CNNPage sendRequest(String request) {
        final String fullSearchPath = String.format(SEARCH_REQUEST, request);
        return  given().baseUri(CNN_SERVER_URL).contentType(ContentType.JSON).get(fullSearchPath).as(CNNPage.class);
    }

    public boolean assertTitles(List<String> titles){
        List<String> titlesFromWeb = cnNresultPage.getTitles();
        return titlesFromWeb.containsAll(titles);
    }

    public boolean assertBodies(List<String> bodies){
        List<String> bodiesFromWeb = cnNresultPage.getBodies().stream().map( s -> s.replaceAll("\\s+","")).collect(Collectors.toList());
        List<String> editedBodyes = bodies.stream().map( s -> s.replaceAll("\\s+", "").replaceAll(" ", "")).collect(Collectors.toList());
        System.out.println(editedBodyes);
        System.out.println(bodiesFromWeb);
        return bodiesFromWeb.containsAll(editedBodyes);
    }


//
//    public Boolean assertBodies(){
//        List<String> bodyFromDTO = cnndto.getBody();
//        List<String> bodyFromWeb = cnNresultPage.getBodies();
//        int i = 0;
//        if(bodyFromDTO.get(i) == null){
//            return true;
//        }
//        if (bodyFromDTO.get(i).equals(bodyFromWeb.get(i))){
//            return assertTitles();
//        }
//
//        return false;
//    }
}
