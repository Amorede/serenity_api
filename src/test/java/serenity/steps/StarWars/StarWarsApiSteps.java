package serenity.steps.StarWars;

import core.ws.AbstractWSConfiguration;
import dto.AllStarWarsUsers;
import io.restassured.RestAssured;
import net.thucydides.core.annotations.Step;

public class StarWarsApiSteps extends AbstractWSConfiguration {


    private static final String SERVER_BASE_URL = "https://swapi.co/api/";
    private static final String ALL_USERS_PATH = "people/";

    @Step
    public AllStarWarsUsers getAllUsers(){
        RestAssured.baseURI = SERVER_BASE_URL;
        return  RestAssured.get(ALL_USERS_PATH).as(AllStarWarsUsers.class);

    }

//    public static void main (String[] str){
//        StarWarsApiSteps steps = new StarWarsApiSteps();
//        AllStarWarsUsers allStarWarsUsers = steps.getAllUsers();
//    }

}
