package serenity.steps.rozetka;

import core.pages.RozetkaMainPage;
import core.pages.RozetkaResultPage;
import core.pages.p.RozetkaWishlist;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

public class RozetkaMainPageController extends ScenarioSteps {

    private RozetkaMainPage rozetkaMainPage;
    private RozetkaResultPage rozetkaResultPage;
    private RozetkaWishlist rozetkaWishlist;

    public RozetkaMainPageController(final Pages pages) {
        rozetkaMainPage = pages.getPage(RozetkaMainPage.class);
        rozetkaResultPage = pages.getPage(RozetkaResultPage.class);
    }


    @Step
    public void openRozetkaPage(final String link) {
        rozetkaMainPage.openAt(link);
    }

    @Step
    public void setTextToSearchField(String text) {
        rozetkaMainPage.setTextToSearchField(text);
    }

    public void clickOnSearchButton() {
        rozetkaMainPage.clickOnSearchButton();
    }

    public void clickOntheAddToWishlist(){
        rozetkaResultPage.clickOnAddToWishList();
    }


    public void setNameOfTheListAndEmail(String nameOftheList, String eMail) {
        rozetkaResultPage.setFirstPopUp(nameOftheList, eMail);
    }

    public void setNameToPopUp(String name) {
        rozetkaResultPage.setSecondPopUp(name);
    }

    public void clickOnWishListPage() {
        rozetkaResultPage.clickOnWishListButton();
    }

    public String getTitleFromTheWishlist(){
        return rozetkaWishlist.getTextFromTitleFromTheWishlist();
    }
}
