package serenity.steps.jsonserver;

import core.ws.AbstractWSConfiguration;
import dto.LukeFilmTitle;
import dto.StarWarsUser;
import dto.StarWarsUserCompound;
import io.restassured.RestAssured;
import net.thucydides.core.annotations.Step;

import java.util.ArrayList;
import java.util.List;

public class LukeGetsFilms extends AbstractWSConfiguration {


    private static final String SWAPI_MAIN_PAGE = "https://swapi.co/api";
    private static final String LUKE_SKYWALKER = "/people/1/";


    @Step
    public StarWarsUser gerLukeSkywalker() {
        RestAssured.baseURI = SWAPI_MAIN_PAGE;
        return RestAssured.get(LUKE_SKYWALKER).as(StarWarsUser.class);
    }


    public StarWarsUserCompound getCompoundUser() {
        LukeGetsFilms steps = new LukeGetsFilms();
        StarWarsUser starWarsUser = steps.gerLukeSkywalker();
        List<String> films = starWarsUser.getFilms();
        List<String> titles = new ArrayList<>();
        for (String item : films) {
            LukeFilmTitle title = RestAssured.get(item).as(LukeFilmTitle.class);
            titles.add(title.toString());
        }
        return new StarWarsUserCompound(starWarsUser, titles);
    }


//    public static void main(String[] args) {
//        LukeGetsFilms steps = new LukeGetsFilms();
//        StarWarsUserCompound compoundUser = steps.getCompoundUser();
//
//    }
}
