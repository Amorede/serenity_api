package serenity.steps.jsonserver;

import core.ws.AbstractWSConfiguration;
import dto.PostDTO;
import io.restassured.http.ContentType;
import net.thucydides.core.annotations.Step;
import utils.JsonUtils;

import static io.restassured.RestAssured.given;

public class JsonServerWebServicesSteps extends AbstractWSConfiguration {

    private static final String JSON_LOCALHOST_SERVER_URL = "http://localhost:3000";
    private static final String POST_API_PATH = "/posts";


    @Step
    public void createNewPost(final PostDTO postDTO) {
        final String postDTOJson = JsonUtils.toJson(postDTO);
        given().baseUri(JSON_LOCALHOST_SERVER_URL)
                .contentType(ContentType.JSON)
                .body(postDTOJson)
                .post(POST_API_PATH);
    }

    @Step
    public PostDTO getCreatedRecource(final String id) {
        final String createdPostApiPath = String.format("%s/%s",POST_API_PATH, id);
        return given().baseUri(JSON_LOCALHOST_SERVER_URL)
                .contentType(ContentType.JSON)
                .get(createdPostApiPath)
                .as(PostDTO.class);
    }

    public void deletePostById(final String id) {
        final String createdPostApiPath = String.format("%s/%s",POST_API_PATH, id);
        given().baseUri(JSON_LOCALHOST_SERVER_URL)
                .contentType(ContentType.JSON)
                .delete(createdPostApiPath);
    }

    public void modifyResourceById(final String id, final PostDTO updateJsonDTO) {
        final String updateJsonBody = JsonUtils.toJson(updateJsonDTO);
        final String modifyPostApiPath = String.format("%s/%s",POST_API_PATH, id);
        given().baseUri(JSON_LOCALHOST_SERVER_URL)
                .contentType(ContentType.JSON)
                .body(updateJsonBody)
                .put(modifyPostApiPath);
    }
}
