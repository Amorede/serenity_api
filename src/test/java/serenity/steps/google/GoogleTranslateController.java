package serenity.steps.google;

import core.pages.GoogleTranslatePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

public class GoogleTranslateController  extends ScenarioSteps {

    private GoogleTranslatePage googleTranslatePage;

    public GoogleTranslateController (final Pages pages){
        this.googleTranslatePage = pages.getPage(GoogleTranslatePage.class);
    }

    @Step
    public void openURL(String url) {
        googleTranslatePage.openAt(url);
    }

    @Step
    public void setSourceLanguage(String language) {
        googleTranslatePage.getLanguagePanel().selectSourceLanguage(language);
    }

    @Step
    public void setTargetLanguage(String language) {
        googleTranslatePage.getLanguagePanel().selectTargetLanguage(language);
    }

    @Step
    public void typeInputWord(String word) {
        googleTranslatePage.getTranslatePanel().typeTextToTranslate(word);
    }

    @Step
    public String getTranslationWord() {
        return googleTranslatePage.getTranslatePanel().getTranslatedText();
    }


}
