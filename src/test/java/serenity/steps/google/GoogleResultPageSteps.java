package serenity.steps.google;

import core.pages.GoogleMainPage;
import core.pages.GoogleResultPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.List;

public class GoogleResultPageSteps extends ScenarioSteps {


    private GoogleResultPage googleResultPage;

    public GoogleResultPageSteps(final Pages pages) {
        googleResultPage = pages.getPage(GoogleResultPage.class);
    }

    @Step
    public List<String> getOpenedPageTitle() {
        return googleResultPage.getSearchResultText();
    }




}
