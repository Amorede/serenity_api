package serenity.steps.sportcheck;

import core.pages.sportcheck.SportcheckMainPage;
import core.panels.FooterHelpfulPanel;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.List;

public class SportcheckMainPageFooterSteps  extends ScenarioSteps {

    private SportcheckMainPage sportcheckMainPage;

    public SportcheckMainPageFooterSteps(final Pages pages){
        this.sportcheckMainPage = pages.getPage(SportcheckMainPage.class);
    }

    @Step
    public void openPage(final String link) {
        sportcheckMainPage.openAt(link);
    }
    

    @Step
    public List<String> getHelpfulLinksList() {
        final FooterHelpfulPanel footerHelpfulPanel= sportcheckMainPage.getFooterHelpfulLinks();
        return footerHelpfulPanel.getHelpfulLinksText();
    }
}
