package serenity;

import core.ws.SWAPIPage;
import io.restassured.http.ContentType;
import serenity.steps.StarWars.StarWarsApiSteps;
import dto.AllStarWarsUsers;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static io.restassured.RestAssured.given;

public class SWAPIController extends ScenarioSteps {


    private StarWarsApiSteps starWarsApiSteps;
    private SWAPIPage swapiPage;

    private final String ROOT_API = "https://swapi.co/api/";
    private final String PEOPLE = "people";



    @Step
    public void openSWAPIURL(final String url) {
            swapiPage.openAt(url);
    }


    public String getRequest(String request) {
        return returnedrequest = given().baseUri(ROOT_API).contentType(ContentType.JSON).get(request).asString();
    }

    public String getReturnedrequest() {
        return returnedrequest;
    }

    public String getResultText(){
        return  webResult = swapiPage.getResultText();
    }

    public String getWebResult() {
        return webResult;
    }


    private String returnedrequest;
    private String webResult;


}
