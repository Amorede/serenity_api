package jbehave.scenariosteps.rozetka;

import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import serenity.steps.rozetka.RozetkaMainPageController;

import java.util.Random;

public class RozetkaPageScenario {

    @Steps
    private RozetkaMainPageController rozetkaMainPageController;



    @Given("open Rozetka web by url: '$link'")
    public void openRozetkaPage(final String link){
        rozetkaMainPageController.openRozetkaPage(link);
    }

    @When("type '$text' in to the search bar")
    public void setTextToSearchField(final String text){
        rozetkaMainPageController.setTextToSearchField(text);
    }

    @Then("go to result Page")
    public void goToResultPage(){
        rozetkaMainPageController.clickOnSearchButton();
    }

    @Then("add first item to the wishList")
    public void addItemToWishList(){
        rozetkaMainPageController.clickOntheAddToWishlist();
    }

    @Then("fill first pop-up with '$NameOfTheList1', and Email")
    public void setNameOfTheListAndEmail(final String nameOftheList){
        String email = Math.abs(new Random().nextDouble()) + "@gmail.com";
        rozetkaMainPageController.setNameOfTheListAndEmail(nameOftheList, email);
    }

    @Then("fill second pop-up with name: '$name'")
    public void setNameToPopUp(final String name){
        rozetkaMainPageController.setNameToPopUp(name);
    }

    @Then("go to the WishlistPage")
    public void goToWishListPage(){
        rozetkaMainPageController.clickOnWishListPage();
    }

    @Then("check that '$product' was added to the list")
    public void checkThatProductWasAddedToWishList(final String product){
        final String actualResult = rozetkaMainPageController.getTitleFromTheWishlist();
        Assert.assertTrue("Product is not in the WishList", actualResult.contains(product));
    }

}
