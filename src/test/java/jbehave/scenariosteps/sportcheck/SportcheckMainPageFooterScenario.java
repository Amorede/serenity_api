package jbehave.scenariosteps.sportcheck;

import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.junit.Assert;
import serenity.steps.sportcheck.SportcheckMainPageFooterSteps;

import java.util.List;

public class SportcheckMainPageFooterScenario {

    @Steps
    private SportcheckMainPageFooterSteps sportcheckMainPageFooterSteps;


    @Given("opened a Sportcheck page, by URL: '$link'")
    public void openSportchekPage(final String link){
        sportcheckMainPageFooterSteps.openPage(link);
    }



    @Then("helpful links should be displayed: '$expectedLinks'")
    public void verifyHelpfulLinks (final List<String> expectedLinks){
        final List<String> actualHelpfulLinks = sportcheckMainPageFooterSteps.getHelpfulLinksList();
        Assert.assertArrayEquals("Helpful Links are not shown at the page!",
                actualHelpfulLinks.toArray(), expectedLinks.toArray());
    }
}
