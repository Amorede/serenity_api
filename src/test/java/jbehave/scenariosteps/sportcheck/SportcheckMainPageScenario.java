package jbehave.scenariosteps.sportcheck;

import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import serenity.steps.sportcheck.SportcheckMainPageSteps;

import java.util.List;

public class SportcheckMainPageScenario {

    @Steps
    private SportcheckMainPageSteps sportcheckMainPageSteps;

    @Given("user is opened a Sportcheck page , using following URL: '$link'")
    public void openSportcheckPage(final String link){
        sportcheckMainPageSteps.openPage(link);
    }

    @When("User clicks 'Shop Categories' button")
    public void clickShopCategoriesButton(){
        sportcheckMainPageSteps.expandShopCategoriesList();
    }

    @Then("categories shoul be displayed: '$categoriesList'")
    public void verifyShopCategoriesList(final List<String> expectedShopCategories){
        final List<String> actualShopCategoriesList = sportcheckMainPageSteps.getShopCategoriesList();
        Assert.assertArrayEquals("There are incorrect 'Shop Categories' displayed!",
                 actualShopCategoriesList.toArray(), expectedShopCategories.toArray());
    }
}
