package jbehave.scenariosteps.google;

import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import serenity.steps.google.GoogleTranslateController;

public class GoogleTranslateScenario extends ScenarioSteps {

    @Steps
    private GoogleTranslateController googleTranslateController;


    @Given("user is opened a '$googleTranslateURL'")
    public void openURL(final String url){
        googleTranslateController.openURL(url);
    }

    @When("user choose '$sourceLanguage' as a source")
    public void setSourceLanguage(final String  language){
        googleTranslateController.setSourceLanguage(language);
    }

    @When("user choose '$targetLanguage' as a target")
    public void setTargetLanguage(final String  language){
            googleTranslateController.setTargetLanguage(language);
    }

    @When("user type '$inputWord' into the source input")
    public void setWordToTranslation(final String  word){
        googleTranslateController.typeInputWord(word);
    }

    @Then("user should see next translated words '$translatedResult'")
    public void checkTranslation(final String translatedResult){
        final String actualWord = googleTranslateController.getTranslationWord();
        Assert.assertEquals("Incorrect translation!", translatedResult, actualWord);
    }
}

//    @When("user fills in following registration info: $parameters")
//    public void doSomething (final ExamplesTable parameters) {
//
//        final User user = parameters.getRowsAs(User.class).get(0);
//    }

