package jbehave.scenariosteps.google;

import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import serenity.steps.google.GooglePageSteps;
import serenity.steps.google.GoogleResultPageSteps;

import java.util.List;

public class GooglePageScenario {

    @Steps
    private GooglePageSteps googlePageSteps;

    @Steps
    private GoogleResultPageSteps googleResultPageSteps;

    @Given("user opened Google page, using following link: '$link'")
    public void openGooglePage(final String url) {
        googlePageSteps.openGooglePage(url);
    }

    @When("set text '$text' to search field")
    public void setTextToSearchField(String text) {
        googlePageSteps.setTextToSearchField(text);
    }

    @Then("check text '$text' is set to search field")
    public void isTextSetToSearchField(String text) {
        Assert.assertEquals("The text is not matched", text, googlePageSteps.getTextFromSearchField());
    }

    @Then("click enter button")
    public void isTextSetToSearchField() {
        googlePageSteps.clickEnterButton();
    }


    @Then("check '$result' search result")
    public void getOpenedPageTitle() {
        final String result = "anacondaz";
        List<String> items = googleResultPageSteps.getOpenedPageTitle();
        Boolean b  =    items.stream()
                        .allMatch( item -> item.contains(result) );

        Assert.assertTrue("There is no required item in the search result list!", b);
    }

//    @Then("the page should be opened")
//    public void isGooglePageOpened() {
//        final String expectedTitle = "Google";
//        Assert.assertEquals("There is incorrect title of opened page!",
//                expectedTitle, googlePageSteps.getOpenedPageTitle());
//    }
//
//    @Then("click on search button")
//    public void searchButtonClick() {
//        googlePageSteps.clickSearchButton();
//    }
}
