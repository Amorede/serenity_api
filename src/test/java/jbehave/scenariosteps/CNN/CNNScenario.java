package jbehave.scenariosteps.CNN;

import core.pages.p.CNNPage;
import core.pages.p.ResultItem;
import core.ws.AbstractWSConfiguration;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import serenity.steps.CNN.CNNController;

import java.util.stream.Collectors;

public class CNNScenario extends AbstractWSConfiguration {



    @Steps
    private CNNController cnnController;
    private CNNPage cnnPage;

    @Given("open CNN page by link '$url'")
    public void openCnnPage(final String url){
        cnnController.openCNNmainPage(url);
    }

    @When("set search with '$search'")
    public void setSearchAndOpenResultPage(final String text){
        cnnController.setSearchtextAndOpenResultPage(text);
    }

    @When("send request to get responce on '$request'")
    public void sendBitcoinRequest(final String request){
        cnnPage = cnnController.sendRequest(request);
    }

    @Then("total amount of articles must be '$amount'")

    public void TotalAmount(String amount){
        final int actualAmount = cnnPage.getMeta().getTotal();
        Assert.assertEquals("Amout is not correct", Integer.toString(actualAmount), amount);
    }

    @Then("compare headers")
    public  void assertHeaders(){
        Assert.assertTrue("Tities are not the same", cnnController.assertTitles(cnnPage.getResult().stream().map(ResultItem::getHeadline).collect(Collectors.toList())));

    }
    @Then("compare bodies")
    public  void assertBody(){
       Assert.assertTrue("Bodies are not the same", cnnController.assertBodies(cnnPage.getResult().stream().map(ResultItem::getBody).collect(Collectors.toList())));

    }
}
