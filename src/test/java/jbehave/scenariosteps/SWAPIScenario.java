package jbehave.scenariosteps;

import serenity.steps.StarWars.StarWarsApiSteps;
import dto.AllStarWarsUsers;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import serenity.SWAPIController;

public class SWAPIScenario {

    @Steps

    private SWAPIController swapiController;

    private StarWarsApiSteps starWarsApiSteps;

    @Given("open page by URL : '$url'")
    public void openSWAPIURL (final String url) {
        swapiController.openSWAPIURL(url);
    }

    @When("type request '$request'")
    public void sendRequest(final String request) {
        swapiController.getRequest(request);
    }

    @Then("assert result on the page and at the API")
    public void assertResults(){
        final String returned = swapiController.getReturnedrequest();
        swapiController.getResultText();
        final String actualResult = swapiController.getWebResult();
        Assert.assertEquals("JSON is not equals!", actualResult, returned);

    }
}
