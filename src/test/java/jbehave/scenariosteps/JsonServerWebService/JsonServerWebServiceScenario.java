package jbehave.scenariosteps.JsonServerWebService;

import dto.PostDTO;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;
import org.unitils.reflectionassert.ReflectionAssert;
import serenity.steps.jsonserver.JsonServerWebServicesSteps;

public class JsonServerWebServiceScenario {


    @Steps
    private JsonServerWebServicesSteps webServicesSteps;


    private static final String REQUIRED_RESOURCE_ID = "if_key";

    @Given("delete post with id '$resource'")
    public void deletePostById(final String id){
        Serenity.setSessionVariable(REQUIRED_RESOURCE_ID).to(id);
        webServicesSteps.deletePostById(id);
    }

    @Then("check if post deleted")
    public void isResourceNotAvaliable(){
        final String id = Serenity.sessionVariableCalled(REQUIRED_RESOURCE_ID);
        final PostDTO createdResource = webServicesSteps.getCreatedRecource(id);
        Assert.assertNull("Author existing", createdResource.getAuthor());
        Assert.assertNull("Title existing", createdResource.getTitle());

    }


    @Given("create new post with following details: $details")
    public void createNewPostWithDetails(final ExamplesTable details){
        final PostDTO postDTO = details.getRowsAs(PostDTO.class).get(0);
        webServicesSteps.createNewPost(postDTO);
    }

    @Given("modified created rosource with id '$resource': $details")
    public void modifyCreatedResource(final String id, final ExamplesTable details){
        final PostDTO updateJsonDTO = details.getRowsAs(PostDTO.class).get(0);
        webServicesSteps.modifyResourceById(id, updateJsonDTO);
    }

    @Then("created post with id '$resource' must be avaliable with next details: $details")
    @Alias("modified post with id '$resource' must be avaliable with next details: $details")
    public void checkCreatedPost(final String id, final ExamplesTable details){
        final PostDTO expectedPostDetails = details.getRowsAs(PostDTO.class).get(0);
        final PostDTO actualResult = webServicesSteps.getCreatedRecource(id);
        ReflectionAssert.assertReflectionEquals("Post was not created!", actualResult, expectedPostDetails);
    }
}
