Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal
!--Then check that 'Huawei Mate 20 Pro' was added to the list

Scenario: scenario description

Given open Rozetka web by url: 'https://rozetka.com.ua/'
When type 'Huawei Mate 20 Pro' in to the search bar
Then go to result Page
Then add first item to the wishList
Then fill first pop-up with 'NameOfTheList1', and Email
Then fill second pop-up with name: 'Тарас Шевченко'
Then go to the WishlistPage
Then check that 'Huawei' was added to the list
