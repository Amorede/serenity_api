<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" lang="en"><head>
<meta charset="utf-8" />
<title>
      SWAPI - The Star Wars API
    </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
<link href="/static/css/bootstrap.css" rel="stylesheet" />
<link href="/static/css/custom.css" rel="stylesheet" />
<link rel="shortcut icon" href="/static/favicon.ico" />

<script async="" src="//www.google-analytics.com/analytics.js"></script><script id="twitter-wjs" src="https://platform.twitter.com/widgets.js"></script><script src="//code.jquery.com/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js" type="text/javascript"></script>

<link type="text/css" rel="stylesheet" href="https://checkout.stripe.com/v3/checkout/button-qpwW2WfkB0oGWVWIASjIOQ.css" /><script charset="utf-8" src="https://platform.twitter.com/js/button.e96bb6acc0f8bda511c0c46a84ee18e4.js"></script></head>
<body>
<nav class="navbar navbar-default" role="navigation">
<div class="container-fluid">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
<span class="sr-only"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<div class="navbar-brand" href="#"><iframe id="twitter-widget-1" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button" style="position: static; visibility: visible; width: 60px; height: 20px;" title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.d30011b0f5ce05b98f24b01d3331b3c1.en.html#dnt=true&amp;id=twitter-widget-1&amp;lang=en&amp;original_referer=https%3A%2F%2Fswapi.co%2F&amp;related=phalt_&amp;size=m&amp;text=SWAPI%20-%20the%20Star%20Wars%20API%20&amp;time=1545937467884&amp;type=share&amp;url=https%3A%2F%2Fswapi.co&amp;via=phalt_" data-url="https://swapi.co"></iframe>
<script type="text/javascript">!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></div>
</div>
<div class="collapse navbar-collapse">
<ul class="nav navbar-nav navbar-right">
<li><a href="/">Home</a></li>
<li><a href="/about">About</a></li>
<li><a href="/documentation">Documentation</a></li>
</ul>
</div>
</div>
</nav>
<div class="container-fluid" style="margin-top: 60px;">
<div class="row center yellow">
<div class="jumbotron">
<h1>SWAPI</h1>
<p class="lead">The Star Wars API</p>
</div>
</div>
<div class="row">
<div class="col-lg-3">
</div>
<div class="col-lg-6 center">
<p>All the Star Wars data you've ever wanted:</p>
<p><b>Planets, Spaceships, Vehicles, People, Films and Species</b></p>
<p>From all <b>SEVEN</b> Star Wars films</p>
<p></p><h4>Now with The Force Awakens data!</h4><p></p>
</div>
<div class="col-lg-3">
</div>
</div>
<div class="row">
<hr />
<div class="col-sm-2 col-lg-2 col-md-2">
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<h1 class="center">
Try it now!
</h1>
<div class="input-group">
<span class="input-group-addon">https://swapi.co/api/</span>
<input id="interactive" type="text" class="form-control" placeholder="people/1/" />
<span class="input-group-btn"><button onclick="interactive_call();return false;" class="btn btn-primary">request</button></span>
</div>
<small>Need a hint? try <a href="#" onclick="update('people/1/');return false;"><i>people/1/</i></a> or <a href="#" onclick="update('planets/3/');return false;"><i>planets/3/</i></a> or <a href="#" onclick="update('starships/9/');return false;"><i>starships/9/</i></a></small>
<p class="lead pad_top">Result:</p>
<div class="well">
<pre id="interactive_output" class="pre-scrollable">{
	"count": 87,
	"next": "https://swapi.co/api/people/?page=2",
	"previous": null,
	"results": [
		{
			"name": "Luke Skywalker",
			"height": "172",
			"mass": "77",
			"hair_color": "blond",
			"skin_color": "fair",
			"eye_color": "blue",
			"birth_year": "19BBY",
			"gender": "male",
			"homeworld": "https://swapi.co/api/planets/1/",
			"films": [
				"https://swapi.co/api/films/2/",
				"https://swapi.co/api/films/6/",
				"https://swapi.co/api/films/3/",
				"https://swapi.co/api/films/1/",
				"https://swapi.co/api/films/7/"
			],
			"species": [
				"https://swapi.co/api/species/1/"
			],
			"vehicles": [
				"https://swapi.co/api/vehicles/14/",
				"https://swapi.co/api/vehicles/30/"
			],
			"starships": [
				"https://swapi.co/api/starships/12/",
				"https://swapi.co/api/starships/22/"
			],
			"created": "2014-12-09T13:50:51.644000Z",
			"edited": "2014-12-20T21:17:56.891000Z",
			"url": "https://swapi.co/api/people/1/"
		},
		{
			"name": "C-3PO",
			"height": "167",
			"mass": "75",
			"hair_color": "n/a",
			"skin_color": "gold",
			"eye_color": "yellow",
			"birth_year": "112BBY",
			"gender": "n/a",
			"homeworld": "https://swapi.co/api/planets/1/",
			"films": [
				"https://swapi.co/api/films/2/",
				"https://swapi.co/api/films/5/",
				"https://swapi.co/api/films/4/",
				"https://swapi.co/api/films/6/",
				"https://swapi.co/api/films/3/",
				"https://swapi.co/api/films/1/"
			],
			"species": [
				"https://swapi.co/api/species/2/"
			],
			"vehicles": [],
			"starships": [],
			"created": "2014-12-10T15:10:51.357000Z",
			"edited": "2014-12-20T21:17:50.309000Z",
			"url": "https://swapi.co/api/people/2/"
		},
		{
			"name": "R2-D2",
			"height": "96",
			"mass": "32",
			"hair_color": "n/a",
			"skin_color": "white, blue",
			"eye_color": "red",
			"birth_year": "33BBY",
			"gender": "n/a",
			"homeworld": "https://swapi.co/api/planets/8/",
			"films": [
				"https://swapi.co/api/films/2/",
				"https://swapi.co/api/films/5/",
				"https://swapi.co/api/films/4/",
				"https://swapi.co/api/films/6/",
				"https://swapi.co/api/films/3/",
				"https://swapi.co/api/films/1/",
				"https://swapi.co/api/films/7/"
			],
			"species": [
				"https://swapi.co/api/species/2/"
			],
			"vehicles": [],
			"starships": [],
			"created": "2014-12-10T15:11:50.376000Z",
			"edited": "2014-12-20T21:17:50.311000Z",
			"url": "https://swapi.co/api/people/3/"
		},
		{
			"name": "Darth Vader",
			"height": "202",
			"mass": "136",
			"hair_color": "none",
			"skin_color": "white",
			"eye_color": "yellow",
			"birth_year": "41.9BBY",
			"gender": "male",
			"homeworld": "https://swapi.co/api/planets/1/",
			"films": [
				"https://swapi.co/api/films/2/",
				"https://swapi.co/api/films/6/",
				"https://swapi.co/api/films/3/",
				"https://swapi.co/api/films/1/"
			],
			"species": [
				"https://swapi.co/api/species/1/"
			],
			"vehicles": [],
			"starships": [
				"https://swapi.co/api/starships/13/"
			],
			"created": "2014-12-10T15:18:20.704000Z",
			"edited": "2014-12-20T21:17:50.313000Z",
			"url": "https://swapi.co/api/people/4/"
		},
		{
			"name": "Leia Organa",
			"height": "150",
			"mass": "49",
			"hair_color": "brown",
			"skin_color": "light",
			"eye_color": "brown",
			"birth_year": "19BBY",
			"gender": "female",
			"homeworld": "https://swapi.co/api/planets/2/",
			"films": [
				"https://swapi.co/api/films/2/",
				"https://swapi.co/api/films/6/",
				"https://swapi.co/api/films/3/",
				"https://swapi.co/api/films/1/",
				"https://swapi.co/api/films/7/"
			],
			"species": [
				"https://swapi.co/api/species/1/"
			],
			"vehicles": [
				"https://swapi.co/api/vehicles/30/"
			],
			"starships": [],
			"created": "2014-12-10T15:20:09.791000Z",
			"edited": "2014-12-20T21:17:50.315000Z",
			"url": "https://swapi.co/api/people/5/"
		},
		{
			"name": "Owen Lars",
			"height": "178",
			"mass": "120",
			"hair_color": "brown, grey",
			"skin_color": "light",
			"eye_color": "blue",
			"birth_year": "52BBY",
			"gender": "male",
			"homeworld": "https://swapi.co/api/planets/1/",
			"films": [
				"https://swapi.co/api/films/5/",
				"https://swapi.co/api/films/6/",
				"https://swapi.co/api/films/1/"
			],
			"species": [
				"https://swapi.co/api/species/1/"
			],
			"vehicles": [],
			"starships": [],
			"created": "2014-12-10T15:52:14.024000Z",
			"edited": "2014-12-20T21:17:50.317000Z",
			"url": "https://swapi.co/api/people/6/"
		},
		{
			"name": "Beru Whitesun lars",
			"height": "165",
			"mass": "75",
			"hair_color": "brown",
			"skin_color": "light",
			"eye_color": "blue",
			"birth_year": "47BBY",
			"gender": "female",
			"homeworld": "https://swapi.co/api/planets/1/",
			"films": [
				"https://swapi.co/api/films/5/",
				"https://swapi.co/api/films/6/",
				"https://swapi.co/api/films/1/"
			],
			"species": [
				"https://swapi.co/api/species/1/"
			],
			"vehicles": [],
			"starships": [],
			"created": "2014-12-10T15:53:41.121000Z",
			"edited": "2014-12-20T21:17:50.319000Z",
			"url": "https://swapi.co/api/people/7/"
		},
		{
			"name": "R5-D4",
			"height": "97",
			"mass": "32",
			"hair_color": "n/a",
			"skin_color": "white, red",
			"eye_color": "red",
			"birth_year": "unknown",
			"gender": "n/a",
			"homeworld": "https://swapi.co/api/planets/1/",
			"films": [
				"https://swapi.co/api/films/1/"
			],
			"species": [
				"https://swapi.co/api/species/2/"
			],
			"vehicles": [],
			"starships": [],
			"created": "2014-12-10T15:57:50.959000Z",
			"edited": "2014-12-20T21:17:50.321000Z",
			"url": "https://swapi.co/api/people/8/"
		},
		{
			"name": "Biggs Darklighter",
			"height": "183",
			"mass": "84",
			"hair_color": "black",
			"skin_color": "light",
			"eye_color": "brown",
			"birth_year": "24BBY",
			"gender": "male",
			"homeworld": "https://swapi.co/api/planets/1/",
			"films": [
				"https://swapi.co/api/films/1/"
			],
			"species": [
				"https://swapi.co/api/species/1/"
			],
			"vehicles": [],
			"starships": [
				"https://swapi.co/api/starships/12/"
			],
			"created": "2014-12-10T15:59:50.509000Z",
			"edited": "2014-12-20T21:17:50.323000Z",
			"url": "https://swapi.co/api/people/9/"
		},
		{
			"name": "Obi-Wan Kenobi",
			"height": "182",
			"mass": "77",
			"hair_color": "auburn, white",
			"skin_color": "fair",
			"eye_color": "blue-gray",
			"birth_year": "57BBY",
			"gender": "male",
			"homeworld": "https://swapi.co/api/planets/20/",
			"films": [
				"https://swapi.co/api/films/2/",
				"https://swapi.co/api/films/5/",
				"https://swapi.co/api/films/4/",
				"https://swapi.co/api/films/6/",
				"https://swapi.co/api/films/3/",
				"https://swapi.co/api/films/1/"
			],
			"species": [
				"https://swapi.co/api/species/1/"
			],
			"vehicles": [
				"https://swapi.co/api/vehicles/38/"
			],
			"starships": [
				"https://swapi.co/api/starships/48/",
				"https://swapi.co/api/starships/59/",
				"https://swapi.co/api/starships/64/",
				"https://swapi.co/api/starships/65/",
				"https://swapi.co/api/starships/74/"
			],
			"created": "2014-12-10T16:16:29.192000Z",
			"edited": "2014-12-20T21:17:50.325000Z",
			"url": "https://swapi.co/api/people/10/"
		}
	]
}</pre>
</div>
</div>
<div class="col-sm-2 col-lg-2 col-md-2">
</div>
</div>
<div class="row pad_bot">
<div class="col-sm-1 col-lg-1 col-md-1">
</div>
<div class="col-sm-3 col-lg-3 col-md-3">
<h4 class="center">What is this?</h4>
<p>The Star Wars API, or "swapi" (Swah-pee) is the world's first quantified and programmatically-accessible data source for all the data from the Star Wars canon universe!</p>
<p>We've taken all the rich contextual stuff from the universe and formatted into something easier to consume with software. Then we went and stuck an API on the front so you can access it all!</p>
</div>
<div class="col-sm-4 col-lg-4 col-md-4">
<h4 class="center">How can I use it?</h4>
<p>All the data is accessible through our HTTP web API. Consult our <a href="/documentation">documentation</a> if you'd like to get started.</p>
<p>Helper libraries for popular programming languages are also provided so you can consume swapi in your favourite programming language, in a style that suits you.</p>
</div>
<div class="col-sm-3 col-lg-3 col-md-3">
<h4 class="center">How can I support this?</h4>
<p>With small donations we can keep swapi running for free, please consider throwing us some beer money to say thank you. With every $10 we can keep the site up for another month!</p>
<p></p><form action="/stripe/donation" method="POST">
<script src="https://checkout.stripe.com/checkout.js" class="stripe-button active" data-key="pk_live_YW9Tb4E41PcwrEOlOkdNuYTc" data-image="https://i.imgur.com/PqkJzx4s.png" data-name="swapi.co" data-description="Donate and keep swapi alive!" data-amount="1000" data-panel-label="Donate" type="text/javascript">
          </script><button type="submit" class="stripe-button-el" style="visibility: visible;"><span style="display: block; min-height: 30px;">Pay with Card</span></button>
</form><p></p>
<p>This project is open source and you can contribute <a href="https://github.com/phalt/swapi">on GitHub</a>.</p>
</div>
<div class="col-sm-1 col-lg-1 col-md-1">
</div>
</div>
<hr />
<script type="text/javascript">
    function update(call){
        jQuery('#interactive').val(call);
        interactive_call();
    }

    function interactive_call(){
        var content = jQuery('#interactive').val()
        if(content == ''){
            content = 'people/1/';
        }
        var call_url = 'api/' + content;
        jQuery.ajax({
      dataType: 'json',
      url: call_url,
      context: document.body
    }).complete(function(data) {
        if(data['status'] == 200){
            var d = jQuery.parseJSON(data['responseText']);
            jQuery('#interactive_output').text(JSON.stringify(d, null, '\t'));
        }
        else if (data['status'] == 404) {
            jQuery('#interactive_output').text(data['status'] + ' ' + data['statusText']);
        }
    });
    }
    </script>
<script type="text/javascript">
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-53299289-3', 'auto');
    ga('send', 'pageview');
  </script>
<div class="row">
<div class="col-lg-12 footer">
<a href="//phalt.co?ref=swapi" target="_blank">© Paul Hallett <script type="text/javascript">
  document.write(new Date().getFullYear());
</script>2018</a>
<span class="pull-right">
<iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-follow-button twitter-follow-button-rendered" style="position: static; visibility: visible; width: 63px; height: 20px;" title="Twitter Follow Button" src="https://platform.twitter.com/widgets/follow_button.d30011b0f5ce05b98f24b01d3331b3c1.en.html#dnt=true&amp;id=twitter-widget-0&amp;lang=en&amp;screen_name=phalt_&amp;show_count=false&amp;show_screen_name=false&amp;size=m&amp;time=1545937467883" data-screen-name="phalt_"></iframe>
<script type="text/javascript">!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<iframe src="//ghbtns.com/github-btn.html?user=phalt&amp;type=follow" allowtransparency="false" frameborder="0" scrolling="0" width="132" height="20"></iframe></span><p></p>
</div>
</div>


</div><iframe scrolling="no" frameborder="0" allowtransparency="true" src="https://platform.twitter.com/widgets/widget_iframe.d30011b0f5ce05b98f24b01d3331b3c1.html?origin=https%3A%2F%2Fswapi.co&amp;settingsEndpoint=https%3A%2F%2Fsyndication.twitter.com%2Fsettings" title="Twitter settings iframe" style="display: none;"></iframe><iframe frameborder="0" allowtransparency="true" src="https://checkout.stripe.com/m/v3/index-3f0dc197837628f45156bf4f7ed0f6ad.html?distinct_id=769813c5-1dc3-bbed-292f-5386a3356bcf" name="stripe_checkout_app" class="stripe_checkout_app" style="z-index: 2147483647; display: none; background: rgba(0, 0, 0, 0.004); border: 0px none transparent; overflow: hidden auto; visibility: visible; margin: 0px; padding: 0px; -webkit-tap-highlight-color: transparent; position: fixed; left: 0px; top: 0px; width: 100%; height: 100%;"></iframe><iframe id="rufous-sandbox" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" style="position: absolute; visibility: hidden; display: none; width: 0px; height: 0px; padding: 0px; border: none;" title="Twitter analytics iframe"></iframe></body></html>